<!-- README.md is generated from README.Rmd. Please edit that file -->
Introduction
============

Short for: "MODule IdentifieR"

Cells are organized in a modular fashion, where essential functions are carried out by functional modules. Modules can be described as clusters of genes, gene products or metabolites that interact together, are co-regulated or physically interacting. Complex diseases rarely arise from a single causal factor but rather from multiple factors, with large individual variation. This leads to dysregulation of parts of functional modules and thereby gives rise to a disease phenotype. The underlying perturbation in parts of the functional modules and the connectivity between them makes up a disease module.

To better understand complex diseases it is crucial to identify disease modules. Genes present in the module might not have a significant impact on the disease on its own. However, the cumulative effect of multiple low penetrance genes could play a major role in the pathogenesis of complex diseases. Network-based approaches could be key in detecting these low penetrance genes, which could potentially be novel biomarkers or therapeutic targets.

Various disease module inference methods have been proposed earlier, using different approaches. Here, MODifieR is presented, an R package that bundles 8 different disease module inference methods into a single package. The 8 methods methods can be classified into 3 different algorithmic classes: seed-based, clique based and co-expression based methods. MODifieR is available under the GNU GPL open source license.

Module inference methods included are:

-   Clique Sum
-   Correlation Clique
-   DIAMoND
-   MCODE
-   Module Discoverer
-   DiffCoEx
-   MODA
-   WGCNA trait-based

Installation
============

MODifieR requires Python 3 with additional Python libraries scipy, sqlite3, numpy and networkx. They are a standard part of Anaconda so installing Anaconda is recommended. It can be downloaded here:

[Anaconda](https://www.anaconda.com/)

In addition, some R packages are required.
------------------------------------------

They can be acquired with the following code:

``` r
if (!requireNamespace("BiocManager", quietly = TRUE))
  install.packages("BiocManager")
BiocManager::install(c("AnnotationDbi",
                       "MODA",
                       "STRINGdb",
                       "limma",
                       "org.Hs.eg.db",
                       "foreach",
                       "doParallel",
                       "Rcpp",
                       "dynamicTreeCut",
                       "flashClust",
                       "reticulate",
                       "plyr",
                       "parallel",
                       "igraph",
                       "WGCNA",
                       "RSQLite",
                       "devtools",
                       "stackoverflow",
                       "preprocessCore",
                       "DESeq2",
                       "edgeR",
                       "openxlsx",
                       "ggplot2",
                       "ggdendro",
                       "ggrepel"),
                     version = "3.8")
```

After that, the package can be installed from github:

``` r
devtools::install_git(url = "https://gitlab.com/Gustafsson-lab/MODifieR.git")
```

Detailed examples and a tutoral can be found in our [Vignette](https://gustafsson-lab.gitlab.io/MODifieR/)

Contributions and future work
-----------------------------

The aim is to continuously develop the package by adding new methods and features. Feature request will be greatly appreciated, as would merge requests.
